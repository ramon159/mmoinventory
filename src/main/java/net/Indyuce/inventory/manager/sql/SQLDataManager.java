package net.Indyuce.inventory.manager.sql;

import net.Indyuce.inventory.MMOInventory;
import net.Indyuce.inventory.inventory.CustomInventoryHandler;
import net.Indyuce.inventory.inventory.InventoryItem;
import net.Indyuce.inventory.inventory.InventoryLookupMode;
import net.Indyuce.inventory.manager.DataManager;
import net.Indyuce.inventory.manager.YamlDataManager;
import org.apache.commons.lang3.Validate;
import org.bukkit.Bukkit;
import org.bukkit.util.io.BukkitObjectOutputStream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Base64;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;

public class SQLDataManager extends DataManager {
    private final SQLManager dataSource = new SQLManager();

    public SQLDataManager() {
        Validate.isTrue(Bukkit.getPluginManager().getPlugin("MythicLib") != null, "MythicLib is required for SQL data storage");
        dataSource.setup(MMOInventory.plugin.getConfig());
    }

    public SQLManager getDataSource() {
        return dataSource;
    }

    @Override
    public void save(CustomInventoryHandler data, boolean autosave) {
        try {

            final Collection<InventoryItem> items = data.getItems(InventoryLookupMode.NORMAL);
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            final BukkitObjectOutputStream bukkitAdapter = new BukkitObjectOutputStream(outputStream);

            // Serialize items
            bukkitAdapter.writeInt(items.size());
            for (InventoryItem entry : items) {
                final int slot = entry.getSlot().getIndex();
                bukkitAdapter.writeInt(slot);
                bukkitAdapter.writeObject(entry.getItemStack());
            }

            // Encode serialized object into to the Base64 format and execute update
            bukkitAdapter.close();
            final String base64 = new String(Base64.getEncoder().encode(outputStream.toByteArray()));

            CompletableFuture.runAsync(() -> {
                try {
                    final Connection connection = dataSource.getConnection();
                    final PreparedStatement prepared = connection.prepareStatement("INSERT INTO mmoinventory_inventories (uuid, inventory, `is_saved`) VALUES(?,?,?) ON DUPLICATE KEY UPDATE inventory = VALUES(`inventory`), `is_saved` = VALUES(`is_saved`);");

                    try {
                        MMOInventory.debug("SQL", "Saving inventory of " + data.getUniqueId());
                        prepared.setString(1, data.getUniqueId().toString());
                        prepared.setString(2, base64);
                        prepared.setInt(3, autosave ? 0 : 1);
                        prepared.executeUpdate();
                    } catch (Throwable throwable) {
                        MMOInventory.plugin.getLogger().log(Level.WARNING, "Could not save player inventory of " + data.getPlayer().getName() + " (" + data.getUniqueId() + ")");
                        throwable.printStackTrace();
                    } finally {

                        // Close statement and connection to prevent leaks
                        prepared.close();
                        connection.close();
                    }
                } catch (SQLException exception) {
                    MMOInventory.plugin.getLogger().log(Level.WARNING, "Could not save player inventory of " + data.getPlayer().getName() + " (" + data.getUniqueId() + "), saving in YAML instead");
                    exception.printStackTrace();

                    // Save in YAML
                    new YamlDataManager().save(data, autosave);
                }
            });

        } catch (IOException exception) {
            MMOInventory.plugin.getLogger().log(Level.WARNING, "Could not save player inventory of " + data.getPlayer().getName() + " (" + data.getUniqueId() + ")");
            exception.printStackTrace();
        }
    }

    @Override
    public void load(CustomInventoryHandler data) {
        new MMOInventoryDataSynchronizer(dataSource, data).fetch();
    }
}
