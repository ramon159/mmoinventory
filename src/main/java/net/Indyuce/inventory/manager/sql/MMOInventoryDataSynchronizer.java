package net.Indyuce.inventory.manager.sql;

import io.lumine.mythic.lib.sql.DataSynchronizer;
import io.lumine.mythic.lib.sql.MMODataSource;
import net.Indyuce.inventory.MMOInventory;
import net.Indyuce.inventory.inventory.CustomInventoryHandler;
import net.Indyuce.inventory.slot.CustomSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.io.BukkitObjectInputStream;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;

public class MMOInventoryDataSynchronizer extends DataSynchronizer {
    private final CustomInventoryHandler data;

    public MMOInventoryDataSynchronizer(MMODataSource dataSource, CustomInventoryHandler data) {
        super("mmoinventory_inventories", "uuid", dataSource, data.getUniqueId());

        this.data = data;
    }

    @Override
    public void loadData(ResultSet result) throws SQLException, IOException, ClassNotFoundException {

        // Decode serialized object
        final byte[] serializedObject = Base64.getDecoder().decode(result.getString("inventory"));
        final ByteArrayInputStream inputStream = new ByteArrayInputStream(serializedObject);
        final BukkitObjectInputStream bukkitAdapter = new BukkitObjectInputStream(inputStream);

        int avail = bukkitAdapter.readInt();
        while (avail-- > 0) {
            final int slot = bukkitAdapter.readInt();
            final ItemStack stack = (ItemStack) bukkitAdapter.readObject();
            final CustomSlot customSlot = MMOInventory.plugin.getSlotManager().get(slot);
            data.setItem(customSlot, stack);
        }

        bukkitAdapter.close();
    }

    @Override
    public void loadEmptyData() {

    }
}
