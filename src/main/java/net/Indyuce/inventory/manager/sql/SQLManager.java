package net.Indyuce.inventory.manager.sql;

import io.lumine.mythic.lib.sql.MMODataSource;
import net.Indyuce.inventory.MMOInventory;

import java.sql.SQLException;

public class SQLManager extends MMODataSource {

    private static final String[] NEW_COLUMNS = new String[]{
            "is_saved", "TINYINT"};

    protected SQLManager() {
        super(MMOInventory.plugin);
    }

    @Override
    public void load() {
        executeUpdateAsync("CREATE TABLE IF NOT EXISTS mmoinventory_inventories (" +
                "uuid VARCHAR(36) NOT NULL," +
                "inventory LONGTEXT," +
                "is_saved TINYINT," +
                "PRIMARY KEY (uuid));");

        // Nullable inventory
        executeUpdate("ALTER TABLE `mmoinventory_inventories` MODIFY `inventory` LONGTEXT;");

        // Add columns that might not be here by default
        for (int i = 0; i < NEW_COLUMNS.length; i += 2) {
            final String columnName = NEW_COLUMNS[i];
            final String dataType = NEW_COLUMNS[i + 1];
            getResultAsync("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'mmoinventory_inventories' AND COLUMN_NAME = '" + columnName + "'", result -> {
                try {
                    if (!result.next())
                        executeUpdate("ALTER TABLE mmoinventory_inventories ADD COLUMN " + columnName + " " + dataType);
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
            });
        }
    }
}
